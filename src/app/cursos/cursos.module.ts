import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrincipalComponent} from './pages/principal/principal.component';
import {ComprarComponent} from './pages/comprar/comprar.component';
import {ListadoComponent} from './pages/listado/listado.component';
import {CursoComponent} from './pages/curso/curso.component';
import {BuscarComponent} from './pages/buscar/buscar.component';
import {CursosRoutingModule} from "./cursos-routing.module";


@NgModule({
  declarations: [
    PrincipalComponent,
    ComprarComponent,
    ListadoComponent,
    CursoComponent,
    BuscarComponent
  ],
  imports: [
    CommonModule,
    CursosRoutingModule
  ]
})
export class CursosModule {
}
