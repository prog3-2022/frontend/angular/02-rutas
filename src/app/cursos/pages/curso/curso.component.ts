import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {switchMap} from "rxjs";

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  styleUrls: ['./curso.component.scss']
})
export class CursoComponent implements OnInit {

  mensaje = '';

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    // Opcion 1 para obtener params de la url
    // const id = this.activatedRoute.snapshot.params['id'];
    // this.mensaje = `Bienvenido al curso: ${id}`;

    // Opcion 2 para obtener params de la url
    this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('id');
      this.mensaje = `Bienvenido al curso: ${id}`;
    });
  }

  volver() {
    this.router.navigate(['/cursos/listado']);
  }

}
